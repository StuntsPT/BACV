### Class #3

#### Bioinformática Aplicada às Ciências da Vida 2023-2024

##### GNU/Linux shell

<img src="common/logo-FCUL.svg" style="background:none; border:none; box-shadow:none;">


© Francisco Pina Martins 2023

---

### Summary

* Why a shell?
* What the shell?
* Working in a text environment
* Navigation
* Common operations

---

### Big data

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Sometimes we need to deal with very large files
* &shy;<!-- .element: class="fragment" -->Our usual tools are just not cut for this
* &shy;<!-- .element: class="fragment" -->Download [this file](C03_assets/env_data.csv.7z) and try to open it in Excel (I dare you!)
* &shy;<!-- .element: class="fragment" -->What went wrong?
* &shy;<!-- .element: class="fragment" -->We could just load the data into R and try to handle it from there
* &shy;<!-- .element: class="fragment" -->Let's do this the efficient way
* &shy;<!-- .element: class="fragment" -->Reboot your machine into GNU/Linux and return here

</div>
<div style="float:right; width:50%;" class="fragment">

</br>

![Cool retro term](C03_assets/terminal.webp)

</div>

---

### Welcome to GNU/Linux

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->The graphical interface is likely not very different from what you are used to
    * &shy;<!-- .element: class="fragment" -->This is, however, built from the ground-up to be a multi-user system
* &shy;<!-- .element: class="fragment" -->You don't **have** to use a terminal to use this OS, but it is where it's true power really lies
* &shy;<!-- .element: class="fragment" -->Open the "file manager" and the "terminal" apps and let's get started

</div>
<div style="float:right; width:50%;" class="fragment">

</br>

![LUbuntu screenshot](C03_assets/lubuntu.jpg)

</div>

---

### What is the shell?

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->A "command line" interface to the Operating System (OS)
    * &shy;<!-- .element: class="fragment" -->Works by issuing commands (programs) followed by "arguments" (space separated)
    * &shy;<!-- .element: class="fragment" -->Shell programs are usually *interoperable* communicating through *text*
* &shy;<!-- .element: class="fragment" -->*bash* (*ksh* *zsh* *fish* *dash* etc...)
* &shy;<!-- .element: class="fragment" -->Can be accessed via a "terminal" app
* &shy;<!-- .element: class="fragment" -->**Mostly** keyboard driven

</div>
<div style="float:right; width:50%;" class="fragment">

</br>

![Deck shell](C03_assets/deck.jpg)

</div>

---

### Keyboard "nerd keys"

<div class="fragment" style="float: right"><img src="C03_assets/Tab-Key.png" /></div>
<div style="float:left; width:80%;">

* "Tab" Key
  * &shy;<!-- .element: class="fragment" -->Completes any command or path
* &shy;<!-- .element: class="fragment" -->↑ and ↓ keys
  * &shy;<!-- .element: class="fragment" -->Navigate previous commands
* &shy;<!-- .element: class="fragment" -->`Home` & `End` move cursor line edges
* &shy;<!-- .element: class="fragment" -->`Ctrl + ←` & `Ctrl + →` move one word at a time

</div>

&shy;<!-- .element: class="fragment" -->![Awesome keyboard](C03_assets/keyboard.png)

---

### Why do I need it?

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Use HPC machines
* &shy;<!-- .element: class="fragment" -->Remote access
* &shy;<!-- .element: class="fragment" -->**Simple** interface
    * &shy;<!-- .element: class="fragment" -->At the cost of memorizing some commands
* &shy;<!-- .element: class="fragment" -->Process automation & reproducibility

</div>
<div style="float:right; width:50%;" class="fragment">

![Data center](C03_assets/data_center.jpg)

</div>

---

### The *prompt*

``user@machine:~$``

``bash-4.1$``

![Prompt image](C03_assets/shell_prompt_small.gif)

[Pimp my prompt](https://www.cyberciti.biz/tips/howto-linux-unix-bash-shell-setup-prompt.html) <!-- .element: class="fragment"-->

* &shy;<!-- .element: class="fragment" -->When the *prompt* ends with `$` it is ready to take a command
* &shy;<!-- .element: class="fragment" -->Try running: `echo "Hello World"`

---

### The filesystem

* &shy;<!-- .element: class="fragment" -->Files and directories (AKA "folders") are organized in a *filesystem*
* &shy;<!-- .element: class="fragment" -->Structured in a "tree-like" fashion
* &shy;<!-- .element: class="fragment" -->The bottom-most directory is the `/` and is called the *filesystem root*
    * &shy;<!-- .element: class="fragment" -->GNU/Linux based OSes are installed at this root
* &shy;<!-- .element: class="fragment" -->Higher level directories build from the root
    * &shy;<!-- .element: class="fragment" -->Eg. `/home/fc29609`
* &shy;<!-- .element: class="fragment" -->File locations in the filesystem are represented by their **path**

&shy;<!-- .element: class="fragment" -->![CentOS filesystem](C03_assets/centos7-filesystem.webp)

---

### Navigation & orientation

<div style="float: right"><img src="C03_assets/shell_prompt_small.gif" /></div>

* &shy;<!-- .element: class="fragment" -->The shell *is* a file manager
* &shy;<!-- .element: class="fragment" -->Where am I?
  * &shy;<!-- .element: class="fragment" -->``$ pwd`` 
  * &shy;<!-- .element: class="fragment" --><font color="red">p</font>rint <font color="red">w</font>orking <font color="red">d</font>irectory 
* &shy;<!-- .element: class="fragment" -->What is in here? 
  * &shy;<!-- .element: class="fragment" -->``$ ls`` 
  * &shy;<!-- .element: class="fragment" --><font color="red">l</font>i<font color="red">s</font>t 

<div class="fragment">

```bash
pwd  # Prints the current path

ls  # Lists current directory contents

ls /  # Lists contents of the filesystem root
```

</div>

---

### Moving around

* &shy;<!-- .element: class="fragment" -->To change the directory we are in we use the `cd` command
  * &shy;<!-- .element: class="fragment" --><font color="red">c</font>all <font color="red">d</font>irectory
  * &shy;<!-- .element: class="fragment" -->Followed by the path to the directory we want to move to

<div class="fragment">

```bash
cd Down<TAB>  # Completes the word "Downloads". Hit ⏎ to enter
cd .. # Takes you back one directory
cd D<TAB><TAB>  # Shows all options starting with "D". Enter one to your choice
cd ~/  # Always takes you back to your 'home'
↑  # Pressing the up arrow reveals the previous command
```

</div>

---

### Working with paths

* &shy;<!-- .element: class="fragment" -->Paths can be absolute (start with "/")
  * &shy;<!-- .element: class="fragment" -->``/home/fc29609/my_pretty_plot.png``
* &shy;<!-- .element: class="fragment" -->Or relative (don't start with "/")
  * &shy;<!-- .element: class="fragment" -->``Downloads/my_awesome_script.R``
  * &shy;<!-- .element: class="fragment" -->``../my_data.csv``
* &shy;<!-- .element: class="fragment" --> Frequent convention examples
  * &shy;<!-- .element: class="fragment" --><font color="red">~</font> -> *home dir*, ex. ``/home/fc29609``
  * &shy;<!-- .element: class="fragment" --><font color="red">./</font> -> "Here"
  * &shy;<!-- .element: class="fragment" --><font color="red">../</font> -> One directory below
  * &shy;<!-- .element: class="fragment" --><font color="red">../../</font> -> Two directories below
* &shy;<!-- .element: class="fragment" -->So, is ``~/my_awesome_plot.png`` a *relative*, or *absolute* path?

---

### More navigation

1. &shy;<!-- .element: class="fragment" -->How do I create a new directory?
2. &shy;<!-- .element: class="fragment" -->How do I erase a directory?
3. &shy;<!-- .element: class="fragment" -->How do I create a new file?
4. &shy;<!-- .element: class="fragment" -->How do I copy a file?
5. &shy;<!-- .element: class="fragment" -->How do I move a file?
6. &shy;<!-- .element: class="fragment" -->How do I erase a file?

<div style="float: right"><img src="C03_assets/shell_prompt_small.gif" /></div>

|||

### Navigation part II

```bash
cd ~/  # Make sure we are on our home directory
mkdir sp
cd sp
mkdir kenny  # Create directories
mkdir cartman
mkdir kyle
mkdir stan
touch cartman/teal_cap  # Create files
touch kyle/green_cap
touch stan/blue_cap
touch kenny/orange_hood
cp cartman/teal_cap ./  # Copy cartman's teal_cap
mv kenny/orange_hood cartman/  # Remove kyle's hood and give it to cartman
cp -r kyle kyle_clone  # The -r switch is necessary to copy directories (and contents)
rm -r kenny  # OMG. (you know the rest)
```

<div class="fragment" style="color:red;">

Be **very**, **very** careful with `rm`, especially when combined with the `-r` switch

</div>

---

### *Wildcards*

* &shy;<!-- .element: class="fragment" -->Special characters that mean something other than their "literal"
  * &shy;<!-- .element: class="fragment" -->"\*" - any character any number of times
  * &shy;<!-- .element: class="fragment" -->"?" - any character, once

<div class="fragment" style="color:red;">


```bash
cd ~/sp
mkdir clones
mkdir cartman_clone
mkdir stan_clone
mv *clone clones  # Get all clones out of here!
```

</div>

---

### Awesome starts here

#### `wget/curl grep sed cat less vim nano head tail cut paste`

Consider this example [data file](C03_assets/small_env_data.csv)?

```bash
cd ~/
mkdir small_data
cd small_data
wget https://gitlab.com/StuntsPT/bacv/-/raw/master/docs/classes/C03_assets/small_env_data.csv  # See also `curl`
head small_env_data.csv
cat small_env_data.csv
tail small_env_data.csv
grep "Forest" small_env_data.csv
sed 's/Swamp/Marsh/' small_env_data.csv
less small_env_data.csv  # Press 'q' to exit less and return to the shell prompt
nano small_env_data.csv  # Press Ctrl+X to exit
```

&shy;<!-- .element: class="fragment" -->Vim has a **very** steep learning curve (worth it!). [Have fun learning it](https://vim-adventures.com/)

---

### *Redirects*

* &shy;<!-- .element: class="fragment" -->The shell can *redirect* the output of any program to a file
* &shy;<!-- .element: class="fragment" -->Represented by the `>` character "pointing" at the path where the output is to be saved
* &shy;<!-- .element: class="fragment" -->`command arg1 arg2 argN > path/to/file.txt`

<div class="fragment">

```bash
cd ~/small_data
head -n 3 small_env_data.csv > micro_env_data.csv  # Resets a file to 0 bytes and writes to it
tail -n 2 small_env_data.csv >> micro_env_data.csv  # Appends text to the end of the file

cat micro_env_data.csv  # Have a look. What alternative programs could you use?
```

</div>

---

### Unix pipe: "|"

* &shy;<!-- .element: class="fragment" -->The shell can also redirect the output of a program as input to the next one
* &shy;<!-- .element: class="fragment" -->Use the `|` character to create a *pipe* between two programs
* &shy;<!-- .element: class="fragment" -->`command1 arg1 arg2 | command2 arg1`

<div class="fragment">

```bash
cd ~/small_data
grep "Wetland" small_env_data.csv | head -n 1  # Show the first Wetland line
grep "Wetland" small_env_data.csv | shuf | head -n 1  # Show a random Wetland line
grep "Wetland" small_env_data.csv | shuf | head -n 5 | cut -d "," -f 2  # Show temperature column only of 5 random Wetland lines
grep "Wetland" small_env_data.csv | shuf | head -n 5 | cut -d "," -f 2 > wetland_temp.txt  # The same as above, but save it to a file
```

</div>

&shy;<!-- .element: class="fragment" -->![Unlimited power](C03_assets/unlimited.gif)

---

### (De)compressing files

<div style="float: right"><img src="C03_assets/shell_prompt_small.gif" /></div>

* &shy;<!-- .element: class="fragment" -->Zip files
    * &shy;<!-- .element: class="fragment" -->`unzip file.zip`
* &shy;<!-- .element: class="fragment" -->7z files
    * &shy;<!-- .element: class="fragment" -->`7z x file.7z`
* &shy;<!-- .element: class="fragment" -->"tar" files
    * &shy;<!-- .element: class="fragment" -->`tar xvf file.tar.gz`
    * &shy;<!-- .element: class="fragment" -->`tar xvf file.tar.bz2`
    * &shy;<!-- .element: class="fragment" -->`tar xvf file.tar.xz`
* &shy;<!-- .element: class="fragment" -->We can also use `tar` to compress
    * &shy;<!-- .element: class="fragment" -->`tar cvfJ file.tar.xz dir_to_compress`

---

### Remote access

<div style="float: left; width:50%">

* &shy;<!-- .element: class="fragment" -->You will now remotely connect to our HPC
    * &shy;<!-- .element: class="fragment" -->10.121.85.51
        * &shy;<!-- .element: class="fragment" -->Meet 'Darwin'
* &shy;<!-- .element: class="fragment" -->You will connect using `ssh` (<font color="red">s</font>ecure <font color="red">sh</font>ell)
    * &shy;<!-- .element: class="fragment" -->`ssh user@host`

<div class="fragment">

```bash
ssh fc29609@10.121.85.51
groups  # What groups does my user belong to?
```

* You will be prompted for your password after the `ssh command`
* Exit with the command `exit` or with `Ctrl+D`

</div>
</div>
<div style="float: right; width:50%">

&shy;<!-- .element: class="fragment" -->![Putty connection interface](C03_assets/ssh.png)

</div>


|||

### Remote access from MS Windows

<div style="float: left; width:60%">

</br>

* &shy;<!-- .element: class="fragment" -->Use a program called [Putty](https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.79-installer.msi)
  * &shy;<!-- .element: class="fragment" -->"Host name" = "10.121.85.51"
  * &shy;<!-- .element: class="fragment" -->You will be asked for your credentials on login

</div>
<div style="float: right; width:40%">

&shy;<!-- .element: class="fragment" -->![Putty connection interface](C03_assets/putty.png)

</div>

---

### Getting data

* &shy;<!-- .element: class="fragment" -->Copy data from the data directory:
  * &shy;<!-- .element: class="fragment" -->`/home/data/rookie`

<div class="fragment">

```bash
cp /home/data/rookie/test.fastq ~/
```

</div>

* &shy;<!-- .element: class="fragment" -->Try to delete this file:

<div class="fragment">

```bash
rm /home/data/rookie/test.fastq
```

</div>

* &shy;<!-- .element: class="fragment" -->Why did it fail?

---

### Permissions

<div style="float: left; width:65%">

* &shy;<!-- .element: class="fragment" -->Every file has "attributes", or "metadata"

<div class="fragment">

```bash 
ls -l /home/data
drwxr-xr-x 2 francisco cicadas   4096 Nov 27 19:54 public
drwxr-x--- 2 francisco bacv      4096 Nov 27 18:59 rookie
drwx------ 2 francisco francisco 4096 Nov 27 19:54 secrets
```

</div>

&shy;<!-- .element: class="fragment" -->![Permission split](C03_assets/perms.png)

</div>
<div style="float: right; width:35%">

* &shy;<!-- .element: class="fragment" -->3 scopes
  * &shy;<!-- .element: class="fragment" -->user
  * &shy;<!-- .element: class="fragment" -->group
  * &shy;<!-- .element: class="fragment" -->others
* &shy;<!-- .element: class="fragment" -->Each of which has 3 "values"
  * &shy;<!-- .element: class="fragment" --><font color="red">r</font>ead (4)
  * &shy;<!-- .element: class="fragment" --><font color="red">w</font>rite (2)
  * &shy;<!-- .element: class="fragment" -->e<font color="red">x</font>ecute (1)

</div>

---

### Copying remote data

* &shy;<!-- .element: class="fragment" -->`scp origin destination`
* &shy;<!-- .element: class="fragment" -->The remote machine is designated as `username@host`
  * &shy;<!-- .element: class="fragment" -->Use `:` between `host` and the remote path
  * &shy;<!-- .element: class="fragment" -->Like `cp`, uses the `-r` switch to copy dirs

<div class="fragment">

```bash
scp username@host:example.fasta ./  # copy from the remote machine to the local machine
scp ./example.fasta username@host:  # copy from the local machine to the remote machine
```

</div>

* &shy;<!-- .element: class="fragment" -->Try copying the `test.fastq` file from Darwin to your local machine!

|||

### Alternative data access

* &shy;<!-- .element: class="fragment" -->Sometimes we just need to *look* at some pictures in the remote server
* &shy;<!-- .element: class="fragment" -->Python comes to the rescue with a webserver (I know right?)
  * &shy;<!-- .element: class="fragment" -->Literally serves a directory as a web page
  * &shy;<!-- .element: class="fragment" -->Especially fun in said directory contains html files
* &shy;<!-- .element: class="fragment" -->Just make sure you are using an unique port

<div class="fragment">

```bash
python3 -m http.server «your student number»
```

</div>

* &shy;<!-- .element: class="fragment" -->Point your web-browser to the displayed address (eg. http://10.121.85.51:29609)
* &shy;<!-- .element: class="fragment" -->Press `Ctrl+C` to 'kill' the webserver

---

### Long tasks

<div style="float: left; width:50%">

* &shy;<!-- .element: class="fragment" -->Once the *terminal* app is closed, all remote running processes are 'killed'
* &shy;<!-- .element: class="fragment" -->We need a way to leave our tasks running in the *background*
  * &shy;<!-- .element: class="fragment" -->`GNU Screen`
  * &shy;<!-- .element: class="fragment" -->`tmux`
* &shy;<!-- .element: class="fragment" -->*Terminal multiplexers*
* &shy;<!-- .element: class="fragment" -->Run *processes* in multiple *windows*

</div>
<div style="float: right; width:50%">

</br>

&shy;<!-- .element: class="fragment" -->![Terminal Multiplexer](C03_assets/multiplexer.png)

</div>

|||

### GNU Screen

```bash
screen -S my_session  # Create a new screen session
htop  # Start a program to leave running in the background
<Ctrl>+a, c  # Create a new window
<Ctrl>+a, <Ctrl>+a  # Cycle thru windows
<Ctrl>+a, d  # Detach session
screen -list  # View screen sessions
screen -dr my_session  # Resume using 'my_session'
exit  # Close the shell (and the screen session if all windows are closed)
```

---

### Integrity checks

* &shy;<!-- .element: class="fragment" -->When dealing with large files it is important to verify that copies are performed correctly
* &shy;<!-- .element: class="fragment" -->For this we use a form of "fingerprinting" or "hashes"
    * &shy;<!-- .element: class="fragment" -->`md5sum`
    * &shy;<!-- .element: class="fragment" -->`sha256sum`
* &shy;<!-- .element: class="fragment" -->Provide a unique value for each file
* &shy;<!-- .element: class="fragment" -->These are then compared to a known hash
    * &shy;<!-- .element: class="fragment" -->Frequently provided in a `.txt` file with the large files

<div class="fragment">

```bash
md5sum test.fastq
sha256sum test.fastq
```

</div>

---

### Finding files

* &shy;<!-- .element: class="fragment" -->Sometimes it is necessary to find missing files
  * &shy;<!-- .element: class="fragment" -->`GNU find`
  * &shy;<!-- .element: class="fragment" -->Provide a filename (full or partial)
  * &shy;<!-- .element: class="fragment" -->Provide a search path
  * &shy;<!-- .element: class="fragment" -->Profit!

<div class="fragment">

```bash
find ~/ -name \*.fastq  # Find all FASTQ files in your home dir
```

</div>

---

### Back to the start

* &shy;<!-- .element: class="fragment" -->Make sure to check out the exercises to perform the task that Excel failed

&shy;<!-- .element: class="fragment" -->![Full circle image](C03_assets/full_circle.png)

---

### References

* [What is the linux shell](https://bash.cyberciti.biz/guide/What_is_Linux_Shell)
* [File permissions in Linux](https://www.redhat.com/sysadmin/linux-file-permissions-explained)
* [`tar` manpage](https://linux.die.net/man/1/tar)
* [Bashcrawl](https://gitlab.com/slackermedia/bashcrawl)
* [Linux commands cheat sheet](C03_assets/linux_commands.pdf)
* [GNU Screen cheat sheet](C03_assets/gnu_screen.pdf)
