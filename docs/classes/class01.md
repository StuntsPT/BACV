### Class #1

#### Bioinformática Aplicada às Ciências da Vida 2023-2024

##### Machine Learning 101 - Classifiers

<img src="common/logo-FCUL.svg" style="background:none; border:none; box-shadow:none;">


© Francisco Pina Martins 2023

---

### Summary

* Overview of Machine Learning
* Types of Machine Learning
* Introduction to Classifiers
* Types of Classifiers
* Evaluation Metrics

---

### Machine Learning

* &shy;<!-- .element: class="fragment" -->A branch of Artificial Intelligence
* &shy;<!-- .element: class="fragment" -->Tools and structures to obtain information from data
* &shy;<!-- .element: class="fragment" -->Systems that acquire and integrate knowledge
    * &shy;<!-- .element: class="fragment" -->Through large-scale observations
    * &shy;<!-- .element: class="fragment" -->Improve and extend themselves
    * &shy;<!-- .element: class="fragment" -->Not programmed with that knowledge

&shy;<!-- .element: class="fragment" -->![Machine learning stock photo](C01_assets/ML.png)

|||

### Machine Learning purpose & examples

* &shy;<!-- .element: class="fragment" -->Predict something unknown based on previous information
    * &shy;<!-- .element: class="fragment" -->Generalization
    * &shy;<!-- .element: class="fragment" -->*Based* on previous information
    * &shy;<!-- .element: class="fragment" -->Not *exactly* like previous information
* &shy;<!-- .element: class="fragment" -->Personalized recommendations
* &shy;<!-- .element: class="fragment" -->Disease diagnosis and drug discovery

&shy;<!-- .element: class="fragment" -->![Netflix logo](C01_assets/netflix.png)

---

### Machine Learning Paradigms

* Unsupervised Learning
* Reinforcement Learning
* &shy;<!-- .element: class="fragment highlight-green" -->Supervised Learning

|||

### Unsupervised Learning

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Rationale:
    * &shy;<!-- .element: class="fragment" -->Algorithm is given unlabeled data
    * &shy;<!-- .element: class="fragment" -->Finds patterns or relationships within the data
* &shy;<!-- .element: class="fragment" -->Applications:
    * &shy;<!-- .element: class="fragment" -->Clustering
    * &shy;<!-- .element: class="fragment" -->Association
    * &shy;<!-- .element: class="fragment" -->Dimensionality Reduction
* &shy;<!-- .element: class="fragment" -->Examples:
    * &shy;<!-- .element: class="fragment" -->PCA
    * &shy;<!-- .element: class="fragment" -->Generative Adversarial Networks

</div>
<div style="float:right; width:50%;" class="fragment">

![A man, wearing a horse mask surfing a car roof, in a city at night. 3d art style.](C01_assets/ai_generated.png)

</div>

|||

###  Reinforcement Learning

<div style="float:left; width:60%;">

* &shy;<!-- .element: class="fragment" -->Rationale:
    * &shy;<!-- .element: class="fragment" -->Agent learns to make decisions by interacting with an environment
    * &shy;<!-- .element: class="fragment" -->Feedback is given in the form of rewards or penalties
* &shy;<!-- .element: class="fragment" -->Applications:
    * &shy;<!-- .element: class="fragment" -->Game playing
    * &shy;<!-- .element: class="fragment" -->Robotics
    * &shy;<!-- .element: class="fragment" -->Autonomous systems
* &shy;<!-- .element: class="fragment" -->Examples:
    * &shy;<!-- .element: class="fragment" -->[Proximal Policy Optimization (PPO)](https://youtu.be/DcYLT37ImBY?feature=shared&t=1226)
    * &shy;<!-- .element: class="fragment" -->Evolution Strategies

</div>
<div style="float:right; width:40%;" class="fragment">

</br>
</br>
</br>

![PPO example](C01_assets/poke_map.gif)

</div>

|||

### Supervised Learning

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Rationale:
    * &shy;<!-- .element: class="fragment" -->Algorithm is *trained* on a labeled dataset
    * &shy;<!-- .element: class="fragment" -->Input data and corresponding output labels are provided
* &shy;<!-- .element: class="fragment" -->Applications: 
    * &shy;<!-- .element: class="fragment" -->Classification
    * &shy;<!-- .element: class="fragment" -->Regression
* &shy;<!-- .element: class="fragment" -->Examples:
    * &shy;<!-- .element: class="fragment" -->Decision Trees
    * &shy;<!-- .element: class="fragment" -->Support Vector Machines (SVM)

</div>
<div style="float:right; width:50%;" class="fragment">

![SVM example](C01_assets/svm_example.svg)
![Decision tree example](C01_assets/simple_decision_tree.svg)

</div>

---

### Generalization

<div style="float:right; width:40%;" class="fragment">

![General supervised approach to ML](C01_assets/learning_01.svg)

</div>
<div style="float:left; width:60%;">

* &shy;<!-- .element: class="fragment" -->Central concept in ML
* &shy;<!-- .element: class="fragment" -->Algorithm learns from training data
    * &shy;<!-- .element: class="fragment" -->Characteristics should map well to the general problem
* &shy;<!-- .element: class="fragment" -->*Induction*
    * &shy;<!-- .element: class="fragment" -->Function *f*
    * &shy;<!-- .element: class="fragment" -->Attempts to minimize *training* error
    * &shy;<!-- .element: class="fragment" -->Maps new examples to predictions
* &shy;<!-- .element: class="fragment" -->Evaluated against a *test set*

</div>

|||

### Test set

<div style="float:right; width:50%;" class="fragment">

![General supervised approach to ML](C01_assets/learning_02.svg)

</div>
<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Frequently composed of 20-30% of the total data
    * &shy;<!-- .element: class="fragment" -->**Not used** for training
    * &shy;<!-- .element: class="fragment" -->**Never, ever** look at it
* &shy;<!-- .element: class="fragment" -->Used to evaluate prediction
* &shy;<!-- .element: class="fragment" -->How often did the predictions match the training set labels?
    * &shy;<!-- .element: class="fragment" -->Training error
        * &shy;<!-- .element: class="fragment" -->Does not necessarily represent the "real" error

</div>

---

### Modelling

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Choice of Algorithm
* &shy;<!-- .element: class="fragment" -->Hyperparameters (provided)
    * &shy;<!-- .element: class="fragment" -->Used to tune parameters
    * &shy;<!-- .element: class="fragment" -->Minimize training error
* &shy;<!-- .element: class="fragment" -->Minimize *testing* error
    * &shy;<!-- .element: class="fragment" -->Use ~10% of the data as *development data*

&shy;<!-- .element: class="fragment" -->![Modelling](C01_assets/ml_modelling.png)

</div>
<div style="float:right; width:50%;">

1. &shy;<!-- .element: class="fragment" -->Split data (YMMV):
    * &shy;<!-- .element: class="fragment" -->70% training
    * &shy;<!-- .element: class="fragment" -->20% testing
    * &shy;<!-- .element: class="fragment" -->10% development
2. &shy;<!-- .element: class="fragment" -->For each set of hyperparameters
    * &shy;<!-- .element: class="fragment" -->Train the model
    * &shy;<!-- .element: class="fragment" -->Compute error rate on *development* data
3. &shy;<!-- .element: class="fragment" -->Choose model with lowest error
4. &shy;<!-- .element: class="fragment" -->Evaluate on test data

</div>

---

### Evaluation metrics

* &shy;<!-- .element: class="fragment" -->Model accuracy can be evaluated graphically
* &shy;<!-- .element: class="fragment" -->*Reciever Operator Curve* (ROC) and *Area Under Curve* (AUC)
* &shy;<!-- .element: class="fragment" -->Sensitivity & Specificity Heatmaps
* &shy;<!-- .element: class="fragment" -->F1 scores

&shy;<!-- .element: class="fragment" -->![Generic ROC](C01_assets/ROC.png)
&shy;<!-- .element: class="fragment" -->![Generic heatmap](C01_assets/HM.png)

---

### Reality gets in the way

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->*Noise* in data
    * &shy;<!-- .element: class="fragment" -->Feature
    * &shy;<!-- .element: class="fragment" -->Label
* &shy;<!-- .element: class="fragment" -->Insufficient training data
* &shy;<!-- .element: class="fragment" -->Algorithm failure
* &shy;<!-- .element: class="fragment" -->*Underfitting*
    * &shy;<!-- .element: class="fragment" -->Algorithm is not "learning enough"
* &shy;<!-- .element: class="fragment" -->*Overfitting*
    * &shy;<!-- .element: class="fragment" -->Algorithm is taking in too many details

</div>
<div style="float:right; width:50%;" class="fragment">

</br>
</br>

![Broken robot](C01_assets/BrokenRobot.png)

</div>

---

### Classifier categories

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->ML classifiers can be applied to perform different kinds of predictions:
    * &shy;<!-- .element: class="fragment" -->Real values (regression)
    * &shy;<!-- .element: class="fragment" -->Yes/No response (Binary Classification)
    * &shy;<!-- .element: class="fragment" -->Multiple category response (Multi-class Classification)
    * &shy;<!-- .element: class="fragment" -->Place objects in order of relevance (Ranking)

</div>
<div style="float:right; width:50%;" class="fragment">

</br>

![Classifiers illustrated](C01_assets/classifiers.svg)

</div>

---

### Types of Classifiers

* &shy;<!-- .element: class="fragment highlight-green" -->Decision Trees
* Naive Bayes
* &shy;<!-- .element: class="fragment highlight-green" -->Support Vector Machines (SVM)
* k-Nearest Neighbors (k-NN)
* Logistic Regression
* Neural Networks

---

### Decision Trees

* &shy;<!-- .element: class="fragment" -->Classic and natural model of ML
    * &shy;<!-- .element: class="fragment" -->"[Divide & Conquer](https://www.geeksforgeeks.org/divide-and-conquer/)" approach
    * &shy;<!-- .element: class="fragment" -->Ask and answer "Yes/No" questions
* &shy;<!-- .element: class="fragment" -->Figure which questions to ask, and in what order
    * &shy;<!-- .element: class="fragment" -->Predict an answer once enough questions are answered

&shy;<!-- .element: class="fragment" -->![Decision tree 2](C01_assets/decision_tree_2.png)

|||

### Decision trees internals

<div style="float:left; width:50%;">

</br>
</br>

* &shy;<!-- .element: class="fragment" -->Training data:
    * &shy;<!-- .element: class="fragment" -->Measurements
        * &shy;<!-- .element: class="fragment" -->Each measurement type is a *feature*
        * &shy;<!-- .element: class="fragment" -->Each measurement is a *feature value*
    * &shy;<!-- .element: class="fragment" -->Correct answer (label)
* &shy;<!-- .element: class="fragment" -->Generates `if/else` statements automatically

</div>
<div style="float:right; width:50%;" class="fragment">

![Classifiers illustrated](C01_assets/training_data.svg)

</div>

---

### Decision trees in practice

* &shy;<!-- .element: class="fragment" -->First, get some data. For this example we will use none other than Fisher's *Iris* dataset from 1936!

<div class="fragment">

```R
set.seed(42)
head(iris)  # Take a peek!

sample_size = floor(0.75*nrow(iris))

# Randomly split data
trainers <- sample(seq_len(nrow(iris)), size = sample_size)
training_set <- iris[trainers,]
test_set <- iris[-trainers,]
```
</div>

* &shy;<!-- .element: class="fragment" -->The dataset is now divided in "training" and "testing"

|||

### Decision trees in practice

* &shy;<!-- .element: class="fragment" -->Install and load all necessary libraries

<div class="fragment">


```R
install.packages("rpart")
install.packages("rpart.plot")
install.packages("cvms")
install.packages("ggimage")
install.packages("rsvg")

library(rpart)
library(rpart.plot)
library(cvms)
```

</div>

|||

### Decision trees in practice

* &shy;<!-- .element: class="fragment" -->Next, create a model and look at it
* &shy;<!-- .element: class="fragment" -->In this example we will be using "Regression Trees"

<div class="fragment">

```R
my_model <- rpart(Species ~ ., data = training_set, method = "class")
# rpart stands for "Recursive Partitioning and Regression Trees
# "." as independent var means "all vars"
# Method is "classification"
# Arguments like "cp", "minsplit", control" or "cost" control model hyperparameters

print(my_model)  # Model results
rpart.plot(my_model, main="Iris classification using Regression tree ML algorithm")  # Model results (pretty)

print(my_model$variable.importance)  # Importance of each variable on the model
```

</div>

|||

### Decision trees in practice

* &shy;<!-- .element: class="fragment" -->Finally, make some predictions!

<div class="fragment">

```R
preds <- predict(my_model, newdata=test_set, type="class")  # Make predictions!
print(preds)

# Generate a confusion matrix
cm <- confusion_matrix(test_set$Species, preds)  # From `caret`
cfm <- as.data.frame(cm$Table)
plot_confusion_matrix(cfm, target_col = "Target", prediction_col = "Prediction", counts_col = "Freq")
# Now we're talking!
```

</div>

* &shy;<!-- .element: class="fragment" -->Not bad for such a simple model!

|||

### Decision trees in practice

* &shy;<!-- .element: class="fragment" -->Classifying another data point

<div class="fragment">

```R
my_flower = as.data.frame(rbind(c(4.6, 3.4, 1.4, 0.3)))
names(my_flower) <- names(iris[,-5])

predict(my_model, newdata=my_flower, type="class")
```

</div>

---

### Decision trees model tuning

* &shy;<!-- .element: class="fragment" -->DTs are typically tuned with the following parameters:
    * &shy;<!-- .element: class="fragment" -->Complexity (`cp=0.01`)
        * &shy;<!-- .element: class="fragment" -->Any split that does not decrease the overall lack of fit by a factor of cp is not attempted
        * &shy;<!-- .element: class="fragment" -->Increasing this value results in simpler trees
        * &shy;<!-- .element: class="fragment" -->Decreasing this value results in complexer trees
        * &shy;<!-- .element: class="fragment" -->`-1` means "infinite" complexity
    * &shy;<!-- .element: class="fragment" -->Minsplit (`minsplit=20`)
        * &shy;<!-- .element: class="fragment" -->Min. # of observations in node for a split to be attempted
    * &shy;<!-- .element: class="fragment" -->Max depth (`maxdepth=30`)
        * &shy;<!-- .element: class="fragment" -->Self explanatory

---

### Support Vector Machines (SVM)

<div style="float:left; width:30%;">

</br>

* &shy;<!-- .element: class="fragment" -->Great when dealing with non-linear data
* &shy;<!-- .element: class="fragment" -->Perform poorly when there are more features than data points

</div>
<div style="float:right; width:70%;" class="fragment">

![Classifiers illustrated](C01_assets/SVMs.svg)

</div>

---

### SVMs in practice

* &shy;<!-- .element: class="fragment" -->Let's do the *Iris* dataset again, but this time using SVM

<div class="fragment">

```R
install.packages("e1071")  # A new library for SVM
library(e1071)

set.seed(42)

sample_size = floor(0.75*nrow(iris))

trainers <- sample(seq_len(nrow(iris)), size = sample_size)
training_set <- iris[trainers,]
test_set <- iris[-trainers,]
```

</div>

* &shy;<!-- .element: class="fragment" -->This is exactly the same we did before. We are simply preparing our dataset.

|||

### SVMs in practice

* &shy;<!-- .element: class="fragment" -->Next, we calculate our model

<div class="fragment">

```R
my_svm = svm(Species ~ ., data = training_set, kernel = "linear", scale = TRUE, type="C")
print(my_svm)
```

</div>

* &shy;<!-- .element: class="fragment" -->Notice the choice of "kernel" and how we need to use the "scale" argument

|||

### SVMs in practice

* &shy;<!-- .element: class="fragment" -->Now we can make predictions, and calculate & plot the confusion matrix

<div class="fragment">

```R
svm_preds <- predict(my_svm, newdata=test_set, type="class")  # Make predictions!
print(svm_preds)

svm_cm <- confusion_matrix(test_set$Species, svm_preds)
svm_cfm <- as.data.frame(svm_cm$Table)
plot_confusion_matrix(svm_cfm, target_col="Target", prediction_col="Prediction", counts_col="Freq")
```

</div>

* &shy;<!-- .element: class="fragment" -->Even better than the decision tree!

|||

### SVMs in practice

* &shy;<!-- .element: class="fragment" -->We can, of course, also classify our special flower

<div class="fragment">

```R
my_flower = as.data.frame(rbind(c(4.6, 3.4, 1.4, 0.3)))
names(my_flower) <- names(iris[,-5])

predict(my_svm, newdata=my_flower, type="class")
```

</div>

|||

### SVMs in practice

* &shy;<!-- .element: class="fragment" -->Now let's try to evaluate our model using ROC

<div class="fragment">

```R
install.packages("pROC")
library(pROC)

roc_svm <- multiclass.roc(response = test_set$Species, predictor=as.numeric(svm_preds))  # Calculate the ROC

plot.roc(roc_svm$rocs[[1]], col = "red", print.auc=TRUE, print.auc.x = 0.5, print.auc.y = 0.5)  # First species
plot.roc(roc_svm$rocs[[2]], col = "blue", add=T, print.auc=TRUE, print.auc.x = 0.5, print.auc.y = 0.4)  # Second species
plot.roc(roc_svm$rocs[[3]], col = "green", add=T, print.auc=TRUE, print.auc.x = 0.5, print.auc.y = 0.3)  # Third species
legend(0.3, 0.2, legend = levels(test_set[,"Species"]), lty = c(1), col = c("red", "blue", "green"))
```

</div>

---

### SVM model tuning

* &shy;<!-- .element: class="fragment" -->SVMs are typically tuned with the following hyperparameters:
    * &shy;<!-- .element: class="fragment" -->Epsilon (`epsilon=0.1`)
    * &shy;<!-- .element: class="fragment" -->Cost (`cost=1`)
* &shy;<!-- .element: class="fragment" -->However, `e1071` comes with a useful `tune.foo()` function!
    * &shy;<!-- .element: class="fragment" -->Provide it with a vector of parameters and all will be tired
    * &shy;<!-- .element: class="fragment" -->Try using `plot(tune.foo())`
* &shy;<!-- .element: class="fragment" -->Also has the respective `tune.foo()$best.model` propriety to return the best fit model!
* &shy;<!-- .element: class="fragment" -->`foo` represents the model name

---

### Decision trees Vs. SVMs

<div style="float:left; width:40%;">

Decision trees:
* &shy;<!-- .element: class="fragment" -->Faster to compute
* &shy;<!-- .element: class="fragment" -->Easier to interpret
* &shy;<!-- .element: class="fragment" -->More robust to outliers

</div>
<div style="float:right; width:60%;" class="fragment">

SVMs:
* &shy;<!-- .element: class="fragment" -->Can deal with higher dimensionality
* &shy;<!-- .element: class="fragment" -->Can handle non-linear classification
* &shy;<!-- .element: class="fragment" -->Often more accurate

</div>

---

### References

* [A course in machine learning](http://ciml.info/)
* [Building Intelligent Interactive Tutors](https://doi.org/10.1016/B978-0-12-373594-2.00007-1)
* [FPGA Based Real Time Classification With Support Vector Machine](https://www.researchgate.net/publication/342987800_FPGA_Based_Real_Time_Classification_With_Support_Vector_Machine)
* [Decision trees in R](https://appsilon.com/r-decision-treees/)
* [SVMs in R](https://www.datacamp.com/tutorial/support-vector-machines-r)
* [Interpreting ROCs](https://www.statology.org/interpret-roc-curve/)
