### Class #1

#### Bioinformática Aplicada às Ciências da Vida 2023-2024

##### Building (good) posters

<img src="common/logo-FCUL.svg" style="background:none; border:none; box-shadow:none;">


© Francisco Pina Martins 2023

---

### Why posters?

* Conferences are usually divided in "Talks" and "Posters"
* Oral presentations will ensure more *exposure*
* Posters can provide more *engagement*
* Early career people will have trouble getting "talks

![Oral presentation](C02_assets/op.png)
