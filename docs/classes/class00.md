### Class #0

#### Bioinformática Aplicada às Ciências da Vida 2023-2024

<img src="common/logo-FCUL.svg" style="background:none; border:none; box-shadow:none;">


© Francisco Pina Martins 2023

---

## Programa

**1ª semana**

* 27-11-2023 – Apresentação. Plotting (and general R) refresh
* 28-11-2023 – Machine learning - Classifiers; Exercícios
* 29-11-2023 – Invited speaker: Telma G. Laurentino - Scientific posters; SEMINÁRIO
* 30-11-2023 – Introdução à linha de comandos GNU/Linux; Exercícios
* 01-12-2023 – **Feriado**

|||

## Programa

**2ª semana**

* 04-12-2023 – The "Omics" revolution: Assemblies; Exercícios
* 05-12-2023 – The "Omics" revolution: Metagenomics; Exercícios
* 06-12-2023 – Flash talks; SEMINÁRIO
* 07-12-2023 – Revisões
* 08-12-2023 – **Feriado**

|||

## Programa

**3ª semana**

* 12-12-2023 – Exame prático

---

## Avaliação

* Exame prático - 50% <!-- .element: class="fragment" data-fragment-index="1" -->
* Flash Talks - 30%  <!-- .element: class="fragment" data-fragment-index="2" -->
* Participação geral nas aulas - 20%. <!-- .element: class="fragment" data-fragment-index="3" -->
* Melhorias:<!-- .element: class="fragment" data-fragment-index="4" -->
    * Avaliação de cada componente é 'guardada'<!-- .element: class="fragment" data-fragment-index="5" -->
    * Melhoria de exame pode ser feita ou no ano seguinte ou na época de recurso<!-- .element: class="fragment" data-fragment-index="6" -->
    * Restantes componentes, só no ano seguinte<!-- .element: class="fragment" data-fragment-index="7" -->

|||

## Exame Prático

* Exercícios similares aos das aulas <!-- .element: class="fragment" data-fragment-index="1" -->
* Totalmente digital <!-- .element: class="fragment" data-fragment-index="2" -->
* Sem consulta <!-- .element: class="fragment" data-fragment-index="3" -->

|||

## Flash talks

* Talks individuais <!-- .element: class="fragment" data-fragment-index="1" -->
* Artigo científico à vossa escolha (sujeito a "aprovação") <!-- .element: class="fragment" data-fragment-index="2" -->
* Apresentação deve ser focada nos conteúdos relacionados com o programa da UC <!-- .element: class="fragment" data-fragment-index="2" -->
* Suporte de 1 poster (projetado) <!-- .element: class="fragment" data-fragment-index="3" -->
* Exposição de 5 min <!-- .element: class="fragment" data-fragment-index="4" -->
* 2 min para perguntas<!-- .element: class="fragment" data-fragment-index="5" -->

|||

## Participação Geral

* Participação nas aulas teóricas <!-- .element: class="fragment" data-fragment-index="1" -->
* Participação na parte prática <!-- .element: class="fragment" data-fragment-index="2" -->

---

## Ordem das apresentações

* Ficheiro com os [nomes dos alunos](C00_assets/nomes.txt) <!-- .element: class="fragment" data-fragment-index="1" -->
* "Random seed" <!-- .element: class="fragment" data-fragment-index="2" -->
* [Script](../scripts/order.R) <!-- .element: class="fragment" data-fragment-index="3" -->
