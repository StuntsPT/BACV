### Class #4

#### Bioinformática Aplicada às Ciências da Vida 2023-2024

##### The Omics Revolution: Assemblies

<img src="common/logo-FCUL.svg" style="background:none; border:none; box-shadow:none;">


© Francisco Pina Martins 2023

---

### Summary

* The history of sequencing
* The current playing field
* High Throughput sequencing applications
* Sequencing data concepts

---

### Evolution of DNA sequencing: Origins

<div style="float:left; width:50%;">

</br>
</br>

* &shy;<!-- .element: class="fragment" -->DNA sequencing started in the 1950s
* &shy;<!-- .element: class="fragment" -->It was not practical or reliable until 1977
    * &shy;<!-- .element: class="fragment" -->Sanger sequencing
        * &shy;<!-- .element: class="fragment" -->Golden standard
        * &shy;<!-- .element: class="fragment" -->Low throughput

</div>
<div style="float:right; width:50%;" class="fragment">

![Frederick Sanger](C04_assets/fsanger.jpg)

</div>

|||

### Evolution of DNA sequencing: Origins

[![First generation sequencing](C04_assets/1st_gen_seq.jpg)](https://doi.org/10.1038/nature24286)

---

### "Low Throughput" Sequence formats

* &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->Some of the most frequent file formats
  * &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->[FASTA](https://zhanglab.ccmb.med.umich.edu/FASTA/)
  * &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->[GB](https://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html)
  * &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->[MEGA](https://www.megasoftware.net/webhelp/walk_through_mega/mega_basics_hc.htm)
  * &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->[ALN](http://meme-suite.org/doc/clustalw-format.html)
  * &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->[NEXUS](http://hydrodictyon.eeb.uconn.edu/eebedia/index.php/Phylogenetics:_NEXUS_Format)
  * &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->[PHYLIP](http://www.phylo.org/tools/obsolete/phylip.html)
  * &shy;<!-- .element: class="fragment" -->&shy;<!-- .element: class="fragment" -->...

|||

### The FASTA format

* &shy;<!-- .element: class="fragment" -->The most used low throughput sequencing format
* &shy;<!-- .element: class="fragment" -->Simplicity is key
    * &shy;<!-- .element: class="fragment" -->A line with a sequence name (identified with a `>`)
    * &shy;<!-- .element: class="fragment" -->One (or more) lines with the respective sequence
* &shy;<!-- .element: class="fragment" -->Can have various extensions:
    * &shy;<!-- .element: class="fragment" -->`fasta`
    * &shy;<!-- .element: class="fragment" -->`fna`
    * &shy;<!-- .element: class="fragment" -->`fsa`

<div class="fragment">

```Text
>Sequence 01
ATCGATCGACTACGCATACGCATACGACTACGCATACGCATACGACTAGCACTCAG
>Sequence 02
GACTCAGACTACGACGACTAGCTAGCTACGTACATTTCGGACGAGACTACGATCGA
```

</div>

---

### Evolution of DNA sequencing: Throughput

<div style="float:left; width:50%;">

</br>

* &shy;<!-- .element: class="fragment" -->Massive Parallel Sequencing makes it's debut in 1996
    * &shy;<!-- .element: class="fragment" -->Pyrosequencing
    * &shy;<!-- .element: class="fragment" -->"Mass marketed" in 2007-2009
* &shy;<!-- .element: class="fragment" -->Multiple competing technologies
* &shy;<!-- .element: class="fragment" -->Became known as "Next Generation Sequencing" - NGS
    * &shy;<!-- .element: class="fragment" -->Characterized by High Throughput and short length

</div>
<div style="float:right; width:50%;" class="fragment">

[![Pyrosequencing](C04_assets/pyro.jpg)](https://doi.org/10.1016/j.tig.2007.12.007)

</div>

|||

### Evolution of DNA sequencing: Throughput

[![Second generation sequencing](C04_assets/2nd_gen_seq.jpg)](https://doi.org/10.1038/nature24286)

|||

### Evolution of DNA sequencing: Throughput

![Sequencing cost over time (2022)](C04_assets/2022_Sequencing_cost_per_Mb.jpg)

|||

### Evolution of DNA sequencing: Throughput

<div style="float:left; width:40%;">

* &shy;<!-- .element: class="fragment" -->This expansion did not come without casualties
    * &shy;<!-- .element: class="fragment" -->Roche 454 (2016)
    * &shy;<!-- .element: class="fragment" -->ABI SOLiD (2015)

&shy;<!-- .element: class="fragment" -->![RIP](C04_assets/RIP.png)

</div>
<div style="float:right; width:60%;" class="fragment">

![Roche 454](C04_assets/454.png)

![ABI SOLiD 5500](C04_assets/ABI-Solid.png)

</div>

|||

### Evolution of DNA sequencing: Throughput

<div style="float:left; width:40%;">

* &shy;<!-- .element: class="fragment" -->Ion Torrent PGM
* &shy;<!-- .element: class="fragment" -->Still used in diagnostics
    * &shy;<!-- .element: class="fragment" -->Low upfront cost
    * &shy;<!-- .element: class="fragment" -->Low cost per run
    * &shy;<!-- .element: class="fragment" -->Short run-times
    * &shy;<!-- .element: class="fragment" -->Low throughput

</div>
<div style="float:right; width:60%;" class="fragment">

![Ion Torrent PGM](C04_assets/ion_torrent.png)

</div>

&shy;<!-- .element: class="fragment" -->![Ion Torrent working](C04_assets/ion-working.png)

---

### The curse of the short reads

<div style="float:left; width:40%;">

* &shy;<!-- .element: class="fragment" -->Short reads (50-150bp long) are difficult to work with
* &shy;<!-- .element: class="fragment" -->Strategies were devised to mitigate this issue
    * &shy;<!-- .element: class="fragment" -->Paired-end sequencing
    * &shy;<!-- .element: class="fragment" -->Mate-paired sequencing

</div>
<div style="float:right; width:60%;" class="fragment">

[![Illumina PE](C04_assets/PE_illumina.jpg)](https://www.illumina.com/science/technology/next-generation-sequencing/plan-experiments/paired-end-vs-single-read.html)
[![Mate pair](C04_assets/Mate_pair.png)](https://www.illumina.com/documents/products/technotes/technote_nextera_matepair_data_processing.pdf)

</div>

---

### "High Throughput" Sequence formats

* &shy;<!-- .element: class="fragment" -->By this time things were more organized
* &shy;<!-- .element: class="fragment" -->Various formats rose and fell
* &shy;<!-- .element: class="fragment" -->[FASTQ](https://knowledge.illumina.com/software/general/software-general-reference_material-list/000002211) is the most common for read output
* &shy;<!-- .element: class="fragment" -->[SAM/BAM](http://samtools.github.io/hts-specs/SAMv1.pdf) is the most used for assembled data (bust sometimes also for read output)

|||

### The FASTQ format

<div style="float:left; width:50%;">

* &shy;<!-- .element: class="fragment" -->Each sequence is represented by 4 lines
    * &shy;<!-- .element: class="fragment" -->@Sequence name
    * &shy;<!-- .element: class="fragment" -->Sequence "base calls"
    * &shy;<!-- .element: class="fragment" -->\+
    * &shy;<!-- .element: class="fragment" -->[Quality values](https://en.wikipedia.org/wiki/FASTQ_format#Format) (as ASCII characters)

</div>
<div style="float:right; width:50%;">

* &shy;<!-- .element: class="fragment" -->Quality is provided as a "Phred score" (Q)
    * &shy;<!-- .element: class="fragment" -->1-40
    * &shy;<!-- .element: class="fragment" -->Higher Q means lower error probability (P)

&shy;<!-- .element: class="fragment" -->`$$ P = 10^{\frac{-Q}{10}} $$`

</div>
<div style="float:left; width:100%;", class="fragment">

```Text
@SRR17045170.1 1 length=100
GGTCAATATGGTGCAAAATAACATGAGTATCACGTGTGATGCGTTACTTTCCGGTAAAAATAAAAATGGTAAATAGAGCGCAATATCGGTAATAACATCAC
+
11AAAFFFFFFFFBGG11B11A1D111D3DA3A000B01D210AE0AFGGDA//A//121B11111B11D221D21111////B1FE/////B22@1B111
@SRR17045170.2 2 length=125
GCAAACGGTTCGCCGTTCTGTTCCATTTGCAGGTACGCCCGCGCATTACCAATAATTGCCTGAATTTTCCCTCGATGGCGGATAATCCCGGCGTCCTGTACCAGTCTTTCGACATCCTCTTCCTGC
+
?BBBBFBBBABDGGGGEFFFGGHF5FGHAE532B552A2A20000BE5BA53B33FG55BF323DFHHB3@FE?1B412///>/3F4B?//</>@<B2G2?222??GHHH//?/?DFGFGGD<G<1
```

</div>

---

### Evolution of DNA sequencing: Extension

<div style="float:left; width:60%;">

* &shy;<!-- .element: class="fragment" -->Oxford Nanopore launched in 2005
* &shy;<!-- .element: class="fragment" -->PacBio launched in 2011
    * &shy;<!-- .element: class="fragment" -->Both popularized in 2015
* &shy;<!-- .element: class="fragment" -->Compared to 2nd gen:
    * &shy;<!-- .element: class="fragment" -->Longer reads
    * &shy;<!-- .element: class="fragment" -->Lower throughput
    * &shy;<!-- .element: class="fragment" -->Higher error rates

</div>
<div style="float:right; width:40%;" class="fragment">

[![Nanopore](C04_assets/nanopore.jpg)](https://nanoporetech.com/applications/dna-nanopore-sequencing)

</div>

&shy;<!-- .element: class="fragment" -->[![PacBio](C04_assets/smrt.jpg)](https://youtu.be/_lD8JyAbwEo)

---

### HTS Applications

* Whole Genome Sequencing
    * &shy;<!-- .element: class="fragment highlight-green" -->Re-sequencing
* Mass targeted sequencing
* Reduced Representation Libraries
* &shy;<!-- .element: class="fragment highlight-blue" -->Metagenomics
* eDNA
* Transcriptomics

---

### Whole Genome Sequencing (WGS)

<div style="float:left; width:65%;">

</br>

* &shy;<!-- .element: class="fragment" -->Sequencing and assembling a species' entire genome
    * &shy;<!-- .element: class="fragment" -->PE & MP sequencing
    * &shy;<!-- .element: class="fragment" -->3rd gen. sequencing
* &shy;<!-- .element: class="fragment" -->***Reads*** are matched to each other
* &shy;<!-- .element: class="fragment" -->Sections of overlapping reads from ***contigs*** of variable ***depth***
* &shy;<!-- .element: class="fragment" -->***Contigs*** are linked into ***scaffolds***
    * &shy;<!-- .element: class="fragment" -->Eventually into chromosomes

</div>
<div style="float:right; width:35%;" class="fragment">

&shy;<!-- .element: class="fragment" -->![Nature Cover Feb 2021](C04_assets/genome.jpg)

</div>

|||

### Whole Genome Sequencing (WGS)

* &shy;<!-- .element: class="fragment" -->*Read*: Digital sequence representation from a sequencing machine
* &shy;<!-- .element: class="fragment" -->*Contig*: A contiguous sequence formed by overlapping reads
* &shy;<!-- .element: class="fragment" -->*Depth*: Number of reads overlapped in a position
    * &shy;<!-- .element: class="fragment" -->*Coverage*: Average depth across a sequence
* &shy;<!-- .element: class="fragment" -->*Scaffold*: A set of ordered contigs (AKA supertig)

&shy;<!-- .element: class="fragment" -->![Assembly](C04_assets/assembly.png)

|||

### Whole Genome Sequencing (WGS)

&shy;<!-- .element: class="fragment" -->[![WGS](C04_assets/WGS.jpg)](https://doi.org/10.1111/mec.14264)

---

### Whole Genome Re-sequencing

* &shy;<!-- .element: class="fragment" -->Compares variable sites between individual genomes
* &shy;<!-- .element: class="fragment" -->Requires a *reference genome*
* &shy;<!-- .element: class="fragment" -->PE reads are *mapped* to a reference

&shy;<!-- .element: class="fragment" -->![SARS-COV-2](C04_assets/sars-cov2.png)

|||

### Whole Genome Re-sequencing

&shy;<!-- .element: class="fragment" -->[![WGR](C04_assets/WGR.jpg)](https://doi.org/10.1111/mec.14264)

---

### Whole Genome Re-sequencing in practice

* &shy;<!-- .element: class="fragment" -->The first thing that needs to be done is obtain data
    * &shy;<!-- .element: class="fragment" -->Normally obtained from sequencing **or** a database
    * &shy;<!-- .element: class="fragment" -->For this class, it is already provided
* &shy;<!-- .element: class="fragment" -->Today we will be mapping SARS-CoV-2 *delta* variant [Illumina sequencing](https://www.ncbi.nlm.nih.gov/sra/?term=SRR15571382) against the original Wuhan strain
* &shy;<!-- .element: class="fragment" -->Log in to *Darwin* and run the following commands:
    * &shy;<!-- .element: class="fragment" -->*Hint*: Start a screen session to make things easier!

<div class="fragment">

```bash
cd ~/  # Make sure we're home
# Keep things tidy
mkdir sars_map
cd sars_map
# Get the data
cp -r /home/data/sars/* ./
# Verify integrity
cd delta
sha256sum -c checksums.txt
```

</div>

* &shy;<!-- .element: class="fragment" -->If everything checks out, you are ready to move on!

---

### Dirty, dirty data!

* &shy;<!-- .element: class="fragment" -->We have data, but is it any good?
* &shy;<!-- .element: class="fragment" -->Meet [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/download.html#fastqc)
* &shy;<!-- .element: class="fragment" -->A quality control application for HTS data
* &shy;<!-- .element: class="fragment" -->It has become so popular that there are several clones
    * &shy;<!-- .element: class="fragment" -->[FastQT](https://github.com/labsquare/fastQt) (`C++` and `Qt5` clone)
    * &shy;<!-- .element: class="fragment" -->[FastQC-rs](https://github.com/fastqc-rs/fastqc-rs) (`rust` clone)

&shy;<!-- .element: class="fragment" -->![FastQC](C04_assets/babraham_bioinformatics.gif)

---

### Running FastQC

<div style="float:left; width:75%">

* &shy;<!-- .element: class="fragment" data-fragment-index="1"-->Can run in *interactive* mode (with a GUI)
  * &shy;<!-- .element: class="fragment" data-fragment-index="2"-->`fastqc`
* &shy;<!-- .element: class="fragment" data-fragment-index="3"-->Can run in batch mode
  * &shy;<!-- .element: class="fragment" data-fragment-index="4"-->`fastqc file1.fastq.gz file2.fastq.gz filen.fastq.gz`
  * &shy;<!-- .element: class="fragment" data-fragment-index="6"-->Gz extension is optional
* &shy;<!-- .element: class="fragment" data-fragment-index="7"-->Outputs an HTML report for each `fastq` file

</div>
<div class="fragment" data-fragment-index="5" style="float:right; width:25%">

![Drake meme](C04_assets/drake.jpg)

</div>

<div style="float:left; width=100%" class="fragment">

```bash
cd ~/sars_map/delta
fastqc *.fastq  # That simple!
# Wait for the analysis to finish         
# Python to the rescue again!
python3 -m http.server 29609
```

</div>

---

### FastQC examples

![Example 1](C04_assets/fastqc_ex01.png)

|||

### FastQC examples

![Example 2](C04_assets/fastqc_ex02.png)

|||

### FastQC examples

![Example 3](C04_assets/fastqc_ex03.png)

|||

### FastQC examples

![Example 4](C04_assets/fastqc_ex04.png)

|||

### FastQC examples

![Example 5](C04_assets/fastqc_ex05.png)

|||

### FastQC examples

![Example 6](C04_assets/fastqc_ex06.png)

|||

### FastQC examples

![Example 7](C04_assets/fastqc_ex07.png)

|||

### FastQC examples

![Example 8](C04_assets/fastqc_ex08.png)

|||

### FastQC examples

![Example 9](C04_assets/fastqc_ex09.png)

|||

### FastQC examples

![Example 10](C04_assets/fastqc_ex10.png)

|||

### FastQC examples

[A tutorial!](https://www.youtube.com/watch?v=bz93ReOv87Y)

---

### Issue correction

* &shy;<!-- .element: class="fragment" -->So... our data isn't perfect.
* &shy;<!-- .element: class="fragment" -->It needs some "trimming"
* &shy;<!-- .element: class="fragment" -->Presenting... [fastp](https://github.com/OpenGene/fastp): an ultrafast FASTQ preprocessor

&shy;<!-- .element: class="fragment" -->[![fastp workflow](C04_assets/fastp.jpg)](https://doi.org/10.1002/imt2.107)

---

### Running `fastp`

```bash
cd ~/sars_map/delta
fastp --detect_adapter_for_pe -f 15 -F 15 -t 10 -T 10 -l 55 --dedup --thread 2 --html delta.fastp.html -i SRR15571382-delta_1.fastq -I SRR15571382-delta_2.fastq -o delta_trimmed_R1.fastq.gz -O delta_trimmed_R2.fastq.gz
```

* &shy;<!-- .element: class="fragment" -->`--detect_adapter_for_pe`: Automatically detect and remove Illumina adapters
* &shy;<!-- .element: class="fragment" -->`-f, -F, -t, and -T`: Trim the 5' and 3' ends on both read pairs
* &shy;<!-- .element: class="fragment" -->`-l 55`: Discard reads with length below 55
* &shy;<!-- .element: class="fragment" -->`--dedup`: Remove duplicate reads
* &shy;<!-- .element: class="fragment" -->`--thread`: Use 2 CPU cores
* &shy;<!-- .element: class="fragment" -->`--html`: Save an HTML report here
* &shy;<!-- .element: class="fragment" -->`-i, -I, -o, and -O`: Input and output files for both read pairs

---

### Did it work?

```bash
cd ~/sars_map/delta
fastqc *.fastq.gz
# Wait for the analysis to finish         
# Python to the rescue once more!
python3 -m http.server 29609
```

* &shy;<!-- .element: class="fragment" -->These parameters are already tuned, so results are good
    * &shy;<!-- .element: class="fragment" -->Normally this step requires some trial and error

---

### Let the games begin

* &shy;<!-- .element: class="fragment" -->Now, we map our "clean" data to a reference genome!
* &shy;<!-- .element: class="fragment" -->For this, we use [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)

&shy;<!-- .element: class="fragment" -->![Bowtie2](C04_assets/bowtie2.png)

---

### Mapping our reads

* Organize our data files

```bash
cd ~/sars_map
mkdir delta_mapping
cd delta_mapping
cp ../*.fasta ./
mv ../delta/*.fastq.gz ./
```

* Start the mapping

```bash
bowtie2-build sars-cov-2-reference.fasta SARS-COV-2  # Build an index
bowtie2 -x SARS-COV-2 -p 2 -1 delta_trimmed_R1.fastq.gz -2 delta_trimmed_R2.fastq.gz -S delta_map.sam  # Map the reads. Careful with the filenames!
samtools view -bS delta_map.sam | samtools sort - -o delta_map.bam  # Convert sam -> bam
samtools index delta_map.bam  # Create an index for downstream programs
```

---

### So, what did I just do?

* &shy;<!-- .element: class="fragment" -->Next, we take a look at our results using "[tablet](https://ics.hutton.ac.uk/tablet/download-tablet/)"
* &shy;<!-- .element: class="fragment" -->Open a new terminal and login to *Darwin*, with the `-Y` switch: `ssh -Y fc29609@10.121.85.51`

<div class="fragment">

```bash
tablet
```

</div>

* &shy;<!-- .element: class="fragment" -->Select the `.bam` file as "input" and the `.fasta` file as a reference
* &shy;<!-- .element: class="fragment" -->Look at the provided GFF3 ([What's this?](https://www.ncbi.nlm.nih.gov/datasets/docs/v1/reference-docs/file-formats/about-ncbi-gff3/)) file to find the "spike" protein coordinates!
  * &shy;<!-- .element: class="fragment" -->*Hint*: `grep`

---

### References

* [DNA sequencing at 40: past, present and future](https://doi.org/10.1038/nature24286)
* [The impact of next-generation sequencing technology on genetics](https://doi.org/10.1016/j.tig.2007.12.007)
* [DNA Sequencing Costs: Data from the NHGRI Genome Sequencing Program](https://www.genome.gov/sequencingcostsdata)
* [The third revolution in sequencing technology](https://doi.org/10.1016/j.tig.2018.05.008)
* [Genome sequencing guide: An introductory toolbox to whole‐genome analysis methods](https://doi.org/10.1002%2Fbmb.21561)
* [Whole-genome sequencing approaches for conservation biology](https://doi.org/10.1111/mec.14264)
* [SARS-COV-2 reference](https://www.ncbi.nlm.nih.gov/nuccore/NC_045512.2)

---

### Emergency (2023-2024)!

Room 1.2.15 had ssh access to *Darwin* locked. This was the attempted solution to work-around the issue, but ultimately also failed due to a likely bug in the used remote filesystem (long paths would get access denied and micromamba failed to install `ncurses`, regardless of the version)

The attempt, however, will stay archived here for historical purposes.

|||

### Emergency

* Type this in your shell:

```bash
"${SHELL}" <(curl -L micro.mamba.pm/install.sh)
```

* Answer "yes" to all questions and allow the default instalation path
* Then type the following:

```bash
micromamba activate
micromamba create -n assembly
micromamba activate assembly
micromamba install sra-tools fastqc fastp bowtie2 samtools tablet -c bioconda -c conda-forge
```

|||

### Emergency Pt2

* Next, get the fastq data from SRA

```bash
cd ~/
mkdir delta
cd delta
fasterq-dump -p SRR15571382
```

* Get this [file](C04_assets/SARS-COV-2.gff3)
* Get [this one](C04_assets/sars-cov-2-reference.fasta) too

* Let's try the class again then!
