# Exercises 03 possible solutions


## Machine Learning

You can find the solutions on [this script](ex10_ML_sols.R)

<!--
## Genome Assemblies

### 1. Login to a remote server

Start a terminal and connect to Darwin:

```bash
ssh fc29609@10.121.85.51

screen -S bonus
```
<!--
### 2. File copying

```bash
cd ~/  # Make sure we are in the home directory

mkdir 

cp -r /home/data/rookie/* ~/rookie_data  # Copy all files

cd rookie_data
sha256sum *

cat rookie_sha256sums.txt  # Compare these to your calculated hashes
```

Find the easter egg yourself!


### 3. De-compression and compression

```bash
cd ~/rookie_data  # Make sure we are in the data directory

tar xvf file.tar.gz
cat file1.txt  # Gzip
```


### 4. Large file manipulation (took me long enough!)

```bash
cd ~/  # Make sure we are back in our home directory

mkdir big_data
cd big_data

wget https://gitlab.com/StuntsPT/BACV/-/raw/master/docs/classes/C03_assets/env_data.csv.7z
7z x env_data.csv.7z

du -sh *  # 82 and 18 MB 

wc -l env_data.csv  # 2700001 lines! No wonder excel fails miserably.
cut -d "," -f 1,3,7 env_data.csv > Temp_per_habitat.csv  # Our small file

head -n 201 env_data.csv > env_data_subsample.csv  # Create our subsample file. Don't forget to send it to the local machine!

R
```

Next we do the following in the R console:

```R
install.packages('BiocManager')
BiocManager::install("pcaMethods")
quit()
```

Next we get the sub sample file from Darwin (do this from a terminal in the local machine):

```bash
scp fc29609@10.121.85.51:big_data/env_data_subsample.csv ~/Documents
```

Finally, we develop a script similar to the one below ([PCA.R](PCA.R)) and place it into Darwin:

```R
library(pcaMethods)

hab_data = read.csv("~/big_data/env_data.csv", sep=",", row.names=1)

hab_pca = pca(hab_data[,-7], method="svd", scale="vector", nPcs=2)

scores_pc1 = hab_pca@scores[, "PC1"]
scores_pc2 = hab_pca@scores[, "PC2"]

png(file="~/big_data/habitat_scores.png", height=2000, width=3000)
plot(scores_pc1, scores_pc2,
     col=factor(hab_data$Habitat),
     ann=FALSE)

title(main="PCA scores plot from habitat data")
title(xlab=sprintf("PC1 %0.1f%% variation explained", hab_pca@R2[1] * 100))
title(ylab=sprintf("PC2 %0.1f%% variation explained", hab_pca@R2[2] * 100))
legend("bottomright", legend=unique(hab_data[,"Habitat"]), col=unique(factor(hab_data[,"Habitat"])), pch=1)
dev.off()

loadings_pc1 = hab_pca@loadings[, "PC1"]
loadings_pc2 = hab_pca@loadings[, "PC2"]

png(file="~/big_data/habitat_loadings.png", height=2000, width=3000)
plot(loadings_pc1, loadings_pc2, pch="", ann=F)
arrows(0, 0, loadings_pc1, loadings_pc2)

text(loadings_pc1, loadings_pc2,
     rownames(hab_pca@loadings), cex=0.9)
title(xlab=sprintf("PC1 %0.1f%% variation explained", round(hab_pca@R2[1] * 100, 2)))
title(ylab=sprintf("PC2 %0.1f%% variation explained", round(hab_pca@R2[2] * 100, 2)))
title(main="PCA loadings plot from habitat data")
dev.off()
```

Back in the shell, we run it using `Rscript` and use *Python* to look at our plots

```bash
cd ~/big_data
Rscript PCA.R
python3 http.server 29609  # Kill the process with "Ctrl+C"
```


### 5. Getting some work done

This one is so much easier compared to last time! Do this in the local machine:

```bash
scp /path/to/ML_script.R fc29609@10.121.85.51:ML_script.R
```

And finally do this in Darwin:


```bash
cd ~/  # Makre sure we are on our home dir
mkdir ML_scripts
mv ML_script.R ML_scripts/
cd ML_scripts

Rscript ML_script.R
```

Make sure all the necessary libraries have been pre-installed in Darwin's R!
-->
