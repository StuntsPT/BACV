# Bioinformática Aplicada às Ciências da Vida 2023-2024

Slides para as aulas da UC de Bioinformática Aplicada às Ciências da Vida 2023-2024 do mestrado de BHA da FCUL.

Slides acessíveis via [gitlab pages](https://stuntspt.gitlab.io/BACV).

Estes slides foram feitos em "[github flavoured markdown](https://guides.github.com/features/mastering-markdown/)" com recurso ao sistema [reveal.js](http://lab.hakim.se/reveal-js/). As páginas de exercícios são geradas com recurso ao [Python-markdown](https://python-markdown.github.io/).
