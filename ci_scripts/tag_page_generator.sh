#!/bin/bash

set -e

git tag -l
for i in $(git tag -l)
do
  _dir="../public/${i}"
  git checkout $i
  mkdir -p ${_dir}
  cp -r docs/* ${_dir}
  cd ${_dir}/classes/exercises
    for j in *.md
    do 
      python -m markdown ${j} -f ${j::-2}html
    done
  cd $CI_PROJECT_DIR
  sed -i '1 i<html>\n<head>\n<link rel="stylesheet" type="text/css" href="styles/main.css">\n</head>\n<body>\n' ${_dir}/classes/exercises/*.html
  _TITLE=$(head -n 1 README.md | tail -c +3)
  _SHORT=$(echo -n $_TITLE | tail -c 4)
  sed -i "s|<body>|<body><header>\n<div class='container'>\n<h1>FE${_SHORT}</h1>\n<h2>${_TITLE}</h2>\n</div>\n</header>\n<div class='container'>\n<section id='main_content'>|" ${_dir}/classes/exercises/*.html
  for j in ${_dir}/classes/exercises/*.html
  do
    echo -e -n '\n</section>\n</div>\n</body>\n</html>' >> ${j}
  done
  cp -r html/styles ${_dir}/classes/exercises/styles
  cp -r html/images ${_dir}/classes/exercises/images
  git stash
done
